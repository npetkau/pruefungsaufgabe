nextflow.enable.dsl = 2
params.outdir = "./report"
params.with_fastqc = false

process fastp {
  publishDir "${params.resistance_outdir}/${fastqfile.getSimpleName()}_fastq/", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastp:0.22.0--h2e03b76_0"
  input:
    path fastqfile
  output:
    path "fastp_fastq/*.fastq", emit: fastqfiles
    path "fastp_report", emit: fastpreport
  script:
    """
    mkdir fastp_fastq
    mkdir fastp_report
    fastp -i ${fastqfile} -o fastp_fastq/${fastqfile.getSimpleName()}_fastp.fastq -h fastp_report/fastp.html -j fastp_report/fastp.json

    """
}

process fastqc {
  publishDir "${params.resistance_outdir}/fastqc/${fastqfile.getSimpleName()}/", mode: 'copy', overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastqc:0.11.9--0"
  input:
    path fastqfile
  output:
    path "fastqc_results", emit: results
  script:
    """
    mkdir fastqc_results
    fastqc ${fastqfile} --outdir fastqc_results
    """
}

process srst2 {
  publishDir "${params.resistance_outdir}/srst2/", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/srst2:0.2.0--py27_2"
  input:
    path fastqfile
  output:
    path "*_genes__CARD_v3.0.8_SRST2__results.txt", emit: textfiles
  script:
    """
    srst2 --input_se ${fastqfile[0]} --output ${fastqfile[0]} --log --gene_db ${fastqfile[1]}
    """
}

process combined {
  publishDir "${params.resistance_outdir}/srst2/", mode: "copy", overwrite: true
  input:
    path textfile
  output:
    path "combined.txt", emit: combined_textfile
  script:
    """
    cat ${textfile} >> combined.txt
    """
}

workflow resistance {
  take:
    fastqfile
    reference
    with_fastqc
    outdir
  
  main: 
    params.resistance_outdir = outdir
    fastp_ch = fastp(fastqfile.flatten())
    if(with_fastqc) {
      fastqc_ch = fastqc(fastp_ch.fastqfiles.flatten())
    }
    srst2_ch = srst2(fastp_ch.fastqfiles.combine(reference))
    combined(srst2_ch.collect())

}

workflow {
  fastqfile = channel.fromPath("rawdata/*.fastq")
  reference = channel.fromPath("data/*.fasta")
  resistance(fastqfile,reference, params.with_fastqc, params.outdir)
}

